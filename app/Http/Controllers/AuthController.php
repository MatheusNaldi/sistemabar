<?php

namespace App\Http\Controllers;

use Auth;
use Flash;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\SignInRequest;

class AuthController extends Controller
{
    /** @var UserService */
    private $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Return the login view
     * GET /login
     *
     * @return Response
     */
    public function login()
    {
        return view('auth.login');
    }

    /**
     * Makes the user authentication
     * POST /signin
     *
     * @param  SignInRequest $request
     *
     * @return Response
     */
    public function signin(SignInRequest $request)
    {
        $user = $this->userService->authenticate($request);

        if (is_null($user)) {
            Flash::error('Login e/ou senha incorretos.');

            return back();
        }

        Auth::login($user, $request->has('remember'));

        return redirect(route('home'));
    }

    /**
     * Logout the user
     * GET /logout
     *
     * @return Response
     */
    public function logout()
    {
        Auth::guard()->logout();

        return redirect(route('login'));
    }
}
