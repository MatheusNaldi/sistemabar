<?php

namespace App\Http\Controllers;

use Flash;
use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;

class ProductController extends Controller
{
    /** @var ProductService */
    private $productService;

    /**
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the Product.
     * GET /produtos
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index()
    {
        $products = $this->productService->all();

        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new Product.
     * GET /produtos/create
     *
     * @return Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created Product in storage.
     * POST /produtos
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $this->productService->create($request);

        Flash::success('Produto salvo com sucesso.');

        return redirect(route('produtos.index'));
    }

    /**
     * Display the specified Product.
     * GET /produtos/{$id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $product = $this->productService->find($id);

        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified Product.
     * GET /produtos/{$id}/edit
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $product = $this->productService->find($id);

        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified Product in storage.
     * PUT|PATCH /produtos/{$id}
     *
     * @param int                $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {
        $this->productService->update($request, $id);

        Flash::success('Produto atualizado com sucesso.');

        return redirect(route('produtos.index'));
    }

    /**
     * Remove the specified product from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->productService->delete($id);

        Flash::success('Produto deletado com sucesso.');

        return redirect(route('produtos.index'));
    }
}
