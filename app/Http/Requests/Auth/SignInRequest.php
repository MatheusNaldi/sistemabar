<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class SignInRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required',
            'password' => 'required',
        ];
    }

    /**
     * Retorna as mensagens de validação.
     *
     * @var array
     */
    public function messages()
    {
        return [
            'login.required' => 'Campo Obrigatório',
            'password.required' => 'Campo Obrigatório',
        ];
    }
}
