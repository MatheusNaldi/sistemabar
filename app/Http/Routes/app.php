<?php

Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);

Route::group(['middleware' => 'guest'], function () {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@login']);
    Route::post('signin', ['as' => 'signin', 'uses' => 'AuthController@signin']);
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

    Route::resource('produtos', 'ProductController');
});
