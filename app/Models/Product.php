<?php

namespace App\Models;

use Eloquent as Model;
use App\Presenters\MediaPresenter;
use App\Presenters\ProductPresenter;
use Laracasts\Presenter\PresentableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes, PresentableTrait;

    public $table = 'products';
    protected $presenter = ProductPresenter::class;
    protected $dates = ['deleted_at'];

    public $fillable = [
        'description',
        'group',
        'subGroup',
        'model',
        'accessory',
        'reference',
        'ncm',
        'size',
        'picture',
        'barCode',
        'costPrice',
        'profit',
        'unit'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'integer',
        'description' => 'string',
        'group'       => 'string',
        'subGroup'    => 'string',
        'model'       => 'string',
        'accessory'   => 'string',
        'reference'   => 'string',
        'ncm'         => 'integer',
        'size'        => 'float',
        'picture'     => 'string',
        'barCode'     => 'string',
        'costPrice'   => 'float',
        'profit'      => 'float',
        'deleted_at'  => 'datetime'
    ];

    public function setProfitAttribute($value)
    {
        $value = $value / 100;

        $this->attributes['profit'] = $value;
    }

    public function getWholesalePriceAttribute()
    {
        return str_replace('.', ',', $this->costPrice * (1 + $this->profit));
    }

    public function getRetailPriceAttribute()
    {
        return str_replace('.', ',', $this->wholesalePrice * 1.5);
    }

    public function getPictureUrlAttribute()
    {
        return asset('storage/products/' . $this->picture);
    }
}
