<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;
use App\Presenters\Traits\PresentDate;

class ProductPresenter extends Presenter
{
    use PresentDate;

    public function costPrice()
    {
        $costPrice = str_replace(',', '.', $this->entity->costPrice);

        return 'R$ ' . number_format($costPrice, 2, ',', '.');
    }

    public function retailPrice()
    {
        $retailPrice = str_replace(',', '.', $this->entity->retailPrice);

        return 'R$ ' . number_format($retailPrice, 2, ',', '.');
    }

    public function wholesalePrice()
    {
        $wholesalePrice = str_replace(',', '.', $this->entity->wholesalePrice);

        return 'R$ ' . number_format($wholesalePrice, 2, ',', '.');
    }

    public function profit()
    {
        return $this->entity->profit * 100 . '%';
    }
}
