<?php

namespace App\Repositories\Contracts;

/**
 * Interface ProductRepository
 * @package namespace App\Repositories\Contracts;
 */
interface ProductRepository extends BaseRepositoryInterface
{
}
