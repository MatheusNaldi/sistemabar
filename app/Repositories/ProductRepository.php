<?php

namespace App\Repositories;

use App\Models\Product;
use InfyOm\Generator\Common\BaseRepository;
use App\Repositories\Contracts\ProductRepository as IProductRepository;

class ProductRepository extends BaseRepository implements IProductRepository
{
    /**
     * Configure the Model
     **/
    public function model()
    {
        return Product::class;
    }
}
