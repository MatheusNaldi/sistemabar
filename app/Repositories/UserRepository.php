<?php

namespace App\Repositories;

use App\Models\User;
use InfyOm\Generator\Common\BaseRepository;
use App\Repositories\Contracts\UserRepository as IUserRepository;

class UserRepository extends BaseRepository implements IUserRepository
{
    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
