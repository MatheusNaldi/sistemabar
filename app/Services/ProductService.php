<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\ProductRepository;

class ProductService
{
    /** @var ProductRepository */
    protected $productRepository;

    /**
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function all()
    {
        return $this->productRepository->all();
    }

    public function create(Request $request)
    {
        $inputs = $request->all();

        $inputs['costPrice'] = str_replace(',', '.', $request->input('costPrice'));
        $inputs['profit'] = str_replace(',', '.', $request->input('profit'));

        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            $name = str_random(60) . '.' . $file->getClientOriginalExtension();

            $file->move(storage_path('app/public/products'), $name);

            $inputs['picture'] = $name;
        }

        return $this->productRepository->create($inputs);
    }

    /**
     * Find the specified product id
     * @param  int $id
     *
     * @return Product
     */
    public function find($id)
    {
        return $this->productRepository->find($id);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->all();

        $inputs['costPrice'] = str_replace(',', '.', $request->input('costPrice'));
        $inputs['profit'] = str_replace(',', '.', $request->input('profit'));

        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            $name = str_random(60) . '.' . $file->getClientOriginalExtension();

            $file->move(storage_path('app/public/products'), $name);

            $inputs['picture'] = $name;
        }

        return $this->productRepository->update($inputs, $id);
    }

    public function delete($id)
    {
        return $this->productRepository->delete($id);
    }
}
