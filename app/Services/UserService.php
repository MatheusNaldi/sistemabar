<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserService
{
    /** @var UserRepository */
    protected $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Authenticate the user
     *
     * @param  Request $request
     *
     * @return Usuario|null
     */
    public function authenticate(Request $request)
    {
        $user = $this->userRepository
            ->findByField('login', $request->login)
            ->first();

        if (is_null($user) || !app('hash')->check($request->password, $user->password)) {
            return null;
        }

        return $user;
    }
}
