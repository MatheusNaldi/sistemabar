<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->string('group')->nullable();
            $table->string('subGroup')->nullable();
            $table->string('model')->nullable();
            $table->string('accessory')->nullable();
            $table->string('reference')->nullable();
            $table->integer('ncm')->nullable();
            $table->float('size')->nullable();
            $table->string('picture')->nullable();
            $table->string('barCode')->nullable();
            $table->float('costPrice')->nullable();
            $table->float('profit')->nullable();
            $table->string('unit')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
