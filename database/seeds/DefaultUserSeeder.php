<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class DefaultUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrador',
            'login' => 'admin',
            'password' => '$2y$10$kKk5n3z8fGTOm/uWqgfOAe4BhaEZyy52QxbChYYkxbBpxUBQDfLC6'
        ]);

        User::create([
            'name' => 'Erika',
            'login' => 'erika',
            'password' => '$2y$10$e.gGFYQWq3VlEPbMsAMVB.1cyLcuFZ1nNj7cVZgsBVyKx9Fc0iife'
        ]);
    }
}
