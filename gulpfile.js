const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const babelify = require('babelify');
const stringify = require('stringify');
const argv = require('yargs').argv;
const env = require('dotenv').load();
const aliasify = require('aliasify');
const aliases = require('./aliases');
const eslintify = require('eslintify');
const LessPluginAutoPrefix = require('less-plugin-autoprefix');

const autoprefix = new LessPluginAutoPrefix({
  browsers: ['last 2 versions']
});

const lessPath = 'resources/assets/less';
const jsPath = 'resources/assets/js/src';
const cssDest = 'public/assets/css';
const jsDest = 'public/assets/js';

gulp.task('browserify', function() {
  gulp.src(jsPath + '/app.js', {
    read: false
  })
    .pipe($.shell('php artisan laroute:generate'))
    .pipe($.bro({
      transform: [
        [eslintify, {'quiet-ignored': true}], babelify, stringify(['.html']), [aliasify, aliases]
      ],
      error: $.notify.onError('Error: <%= error.message %>')
    }))
    .pipe($.plumber({
      errorHandler: $.notify.onError('Error: <%= error.message %>')
    }))
    .pipe(gulp.dest(jsDest))
    .pipe($.rename({
      suffix: '.min'
    }))
    .pipe($.sourcemaps.init({
      identityMap: true,
      loadMaps: true,
      debug: true
    }))
    .pipe($.uglify())
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest(jsDest))
    .pipe($.notify('Build de javascript finalizada'));
});

gulp.task('lint', () => {
  return gulp.src(['gulpfile.js', 'resources/assets/js/src/**/*.js'])
    .pipe($.eslint())
    .pipe($.eslint.format());
});

gulp.task('css', function() {
  gulp.src(lessPath + '/app.less')
    .pipe($.plumber({
      errorHandler: $.notify.onError('Error: <%= error.message %>')
    }))
    .pipe($.less({
      plugins: [autoprefix]
    }))
    .pipe(gulp.dest(cssDest))
    .pipe($.rename({
      suffix: '.min'
    }))
    .pipe($.csso())
    .pipe(gulp.dest(cssDest))
    .pipe($.notify('Build de CSS finalizada'));
});

gulp.task('libs', function() {
  gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/admin-lte/dist/js/app.js',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/selectize/dist/js/standalone/selectize.js',
    'node_modules/datatables/media/js/jquery.dataTables.js',
    'node_modules/datatables-bootstrap3-plugin/media/js/datatables-bootstrap3.js',
  ])
  .pipe($.concat('libs.js'))
  .pipe(gulp.dest(jsDest))
  .pipe($.rename('libs.min.js'))
  .pipe($.uglify())
  .pipe(gulp.dest(jsDest))
  .pipe($.notify('Libs finalizado!'));
});

gulp.task('watch', ['browserify', 'css', 'libs'], function() {
  gulp.watch(jsPath + '/**/*.js', ['browserify']);
  gulp.watch(lessPath + '/**/*.less', ['css']);
  gulp.watch(jsPath + '/libs.js', ['libs']);
});

gulp.task('default', function() {
  gulp.start(['browserify', 'css', 'libs']);
});

gulp.task('copy', function() {
  gulp.src([
    'node_modules/bootstrap/fonts/*',
    'node_modules/font-awesome/fonts/*'
  ])
  .pipe($.copy('public/assets/fonts', {
    prefix: 3
  }))
});

