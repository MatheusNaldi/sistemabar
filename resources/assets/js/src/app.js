// Events
const PreviewImageInput = require('./events/PreviewImageInput');
const ProductPriceRefresh = require('./events/ProductPriceRefresh');

// Functions
const MakeDatePickerOnEach = require('./functions/MakeDatePickerOnEach');
const MakeDataTableOnEach = require('./functions/MakeDataTableOnEach');
const MakeSelectizeOnEach = require('./functions/MakeSelectizeOnEach');

// Plugins
const TableLink = require('table-link');

MakeDatePickerOnEach();
MakeDataTableOnEach();
MakeSelectizeOnEach();

TableLink.init();
PreviewImageInput.init();
ProductPriceRefresh.init();
