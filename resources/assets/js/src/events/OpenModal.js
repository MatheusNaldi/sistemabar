//função para abrir modal
$(document).on('click', 'a[data-modal-open]', function(e) {
  const target = e.target;
  const button = target.tagName.toLowerCase() == 'a' ? $(target) : $(target).closest('a');
  e.preventDefault();
  e.stopPropagation();

  $(button.data('target') + ' .modal-dialog').load(button.data('href'), function(responseTxt, statusTxt, xhr) {
    if(statusTxt == 'error') {
      //alert('Error: ' + xhr.status + ': ' + xhr.statusText);
      alert('Ocorreu um erro ao tentar abrir esta pagina!');
    } else {
      $(button.data('target')).modal('show');
      $(document).trigger('needs-application-restart');
    }
  });
});
