function PreviewImageInput() {
  $('[data-preview-img]').change(function(event){
    const target = event.target;

    if (this.files && this.files[0]) {
      const reader = new FileReader();
      reader.onload = function (e) {
        const previewImg = document.querySelector(target.dataset.previewImg);
        previewImg.src = e.target.result;
      }
      reader.readAsDataURL(this.files[0]);
    } else {
      $(target.dataset.previewImg).prop('src', '');
    }
  });
};

exports.init = PreviewImageInput;
