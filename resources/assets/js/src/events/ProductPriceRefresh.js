function ProductPriceRefresh() {
  const costPriceInput = document.getElementById('costPrice');
  const profitInput = document.getElementById('profit');
  const retailPriceInput = document.getElementById('retailPrice');
  const wholesalePriceInput = document.getElementById('wholesalePrice');

  costPriceInput.addEventListener('change', function (event) {
    const target = event.target;

    if (target.value == '') {
      target.value = '0';
      retailPriceInput.value = '0';
      wholesalePriceInput.value = '0';
      return;
    }

    if (profitInput.value == '0') {
      retailPriceInput.value = '0';
      wholesalePriceInput.value = '0';
      return;
    }

    const costPriceValue = target.value.replace(',', '.');
    const profitValue = profitInput.value.replace(',', '.');

    let wholesaleValue = costPriceValue * ( 1 + (profitValue / 100));
    wholesaleValue = (Math.round(wholesaleValue * 100) / 100).toFixed(2);

    let retailValue = wholesaleValue * 1.5;
    retailValue = (Math.round(retailValue * 100) / 100).toFixed(2);

    wholesalePriceInput.value = wholesaleValue.toString().replace('.', ',');
    retailPriceInput.value = retailValue.toString().replace('.', ',');
  });

  profitInput.addEventListener('change', function (event) {
    const target = event.target;

    if (target.value == '') {
      target.value = '0';
      retailPriceInput.value = '0';
      wholesalePriceInput.value = '0';
      return;
    }

    if (costPriceInput.value == '0') {
      retailPriceInput.value = '0';
      wholesalePriceInput.value = '0';
      return;
    }

    const costPriceValue = costPriceInput.value.replace(',', '.');
    const profitValue = target.value.replace(',', '.');

    let wholesaleValue = costPriceValue * ( 1 + (profitValue / 100));
    wholesaleValue = (Math.round(wholesaleValue * 100) / 100).toFixed(2);

    let retailValue = wholesaleValue * 1.5;
    retailValue = (Math.round(retailValue * 100) / 100).toFixed(2);

    wholesalePriceInput.value = wholesaleValue.toString().replace('.', ',');
    retailPriceInput.value = retailValue.toString().replace('.', ',');
  });

  retailPriceInput.addEventListener('change', function (event) {
    const target = event.target;

    if (target.value == '') {
      target.value = '0';
      profitInput.value = '0';
      wholesalePriceInput.value = '0';
      return;
    }

    if (costPriceInput.value == '0') {
      target.value = '0';
      wholesalePriceInput.value = '0';
      return;
    }

    const costPriceValue = costPriceInput.value.replace(',', '.');
    const retailPriceValue = target.value.replace(',', '.');

    let wholesaleValue = retailPriceValue / 1.5;
    wholesaleValue = (Math.round(wholesaleValue * 100) / 100).toFixed(2);

    let profitValue = ((wholesaleValue / costPriceValue) - 1) * 100;
    profitValue = (Math.round(profitValue * 100) / 100).toFixed(2);

    profitInput.value = profitValue.toString().replace('.', ',');
    wholesalePriceInput.value = wholesaleValue.toString().replace('.', ',');
  });

  wholesalePriceInput.addEventListener('change', function (event) {
    const target = event.target;

    if (target.value == '') {
      target.value = '0';
      retailPriceInput.value = '0';
      profitInput.value = '0';
      return;
    }

    if (costPriceInput.value == '0') {
      retailPriceInput.value = '0';
      target.value = '0';
      return;
    }

    const costPriceValue = costPriceInput.value.replace(',', '.');
    const wholesaleValue = target.value.replace(',', '.');

    let profitValue = ((wholesaleValue / costPriceValue) - 1) * 100;
    profitValue = (Math.round(profitValue * 100) / 100).toFixed(2);

    let retailValue = wholesaleValue * 1.5;
    retailValue = (Math.round(retailValue * 100) / 100).toFixed(2);

    profitInput.value = profitValue.toString().replace('.', ',');
    retailPriceInput.value = retailValue.toString().replace('.', ',');
  });
};

exports.init = ProductPriceRefresh;
