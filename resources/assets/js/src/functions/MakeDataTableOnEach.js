function MakeDataTableOnEach() {
  const dataTableSelector = $('.data-table');
  if (dataTableSelector.length) {
    dataTableSelector.each(function(n, el) {
      let options = {};
      if (!el.classList.contains('dataTable')) {
        if (el.dataset.checkboxTable == 'true') {
          options = {
            columnDefs: [{
              'targets': 0,
              'orderable': false
            }],
          };
        }
        options.paging = !el.classList.contains('no-paging');
        options.info = !el.classList.contains('no-info');
        options.filter = !el.classList.contains('no-filter');

        $(el).DataTable(options);
      }
    });
  }
}

module.exports = MakeDataTableOnEach;
