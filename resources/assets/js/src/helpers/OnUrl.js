const in_array = require('in_array');
/**
 * This function executes an callback if the url parameter matches the
 * current url from 'location.pathname' or 'location.href' if strict
 * mode is on.
 *
 * @param {String|RegExp} url - The url or pathname that should fire the event
 * @param {Function} callback - The function that will be called if the
 * url matches the url parameter
 * @param {Bool} [strict=true] - If should compare to the pathname or
 * the complete url. The default is true.
 */
module.exports = function OnUrl(url, callback, strict) {
  strict = typeof strict === 'undefined' ? false : strict;

  const path = location[strict ? 'href' : 'pathname'];

  if (url.constructor === Array && in_array(path, url)) {
    return callback();
  }

  if(url instanceof RegExp && url.test(path)) {
    return callback();
  }

  if (path === url) {
    return callback();
  }
};
