<?php

use Illuminate\Support\Collection;

Form::macro('textField', function ($name, $label = null, $value = null, $attributes = []) {
    $element = Form::text($name, $value ? $value : old($name), field_attributes($name, $attributes));

    return field_wrapper($name, $label, $element);
});

Form::macro('passwordField', function ($name, $label = null, $attributes = []) {
    $element = Form::password($name, field_attributes($name, $attributes));

    return field_wrapper($name, $label, $element);
});

Form::macro('numberField', function ($name, $label = null, $value = null, $attributes = []) {
    $element = Form::number($name, $value ? $value : old($name), field_attributes($name, $attributes));

    return field_wrapper($name, $label, $element);
});

Form::macro('selectField', function ($name, $label = null, $options = [], $value = null, $attributes = []) {
    $element = Form::select($name, $options, $value ? $value : old($name), field_attributes($name, $attributes));

    return field_wrapper($name, $label, $element);
});

Form::macro('selectFieldWithEmptyOption', function ($name, $label = null, $options = [], $value = null, $attributes = []) {
    if (is_array($options)) {
        $options = array_merge(['' => 'Selecione...'], $options);
    } elseif ($options instanceof Collection) {
        $options->prepend('Selecione...', '');
    }

    $element = Form::select($name, $options, $value ? $value : old($name), field_attributes($name, $attributes));

    return field_wrapper($name, $label, $element);
});

Form::macro('dateField', function ($name, $label = null, $value = null, $attributes = []) {
    $element = '<div class="input-group date">';
    $element .= Form::dateTime($name, $value ? $value : old($name), field_attributes($name, $attributes));
    $element .= '<span class="input-group-addon">';
    $element .= '<i class="fa fa-calendar"></i>';
    $element .= '</span>';
    $element .= '</div>';

    return field_wrapper($name, $label, $element);
});

Form::macro('timeField', function ($name, $label = null, $value = null, $attributes = []) {
    $element = '<div class="input-group date">';
    $element .= Form::text($name, $value ? $value : old($name), field_attributes($name, $attributes));
    $element .= '<span class="input-group-addon">';
    $element .= '<i class="fa fa-clock-o"></i>';
    $element .= '</span>';
    $element .= '</div>';

    return field_wrapper($name, $label, $element);
});

Form::macro('radioInline', function ($name, $label = null, $options = [], $checked = null, $attributes = []) {
    $out = $label ? '<label for="' . $name . '">' . $label . '</label>' : '';

    $values = array_keys($options);

    $out .= '<div class="radio">';
    foreach ($values as $value) {
        $isChecked = old($name, $checked) == $value;
        $out .= '<label class="radio-inline">';
        $out .= Form::radio($name, $value, $isChecked, $attributes) . $options[$value];
        $out .= '</label>';
    }
    $out .= '</div>';
    return $out;
});

Form::macro('checkboxField', function ($name, $label = null, $value = 1, $checked = null, $attributes = [], $labelClass = '') {

    $out = '<div class="checkbox">';
    $out .= '<label class="' . $labelClass . '">';
    $out .= Form::checkbox($name, $value, $checked ? $checked : old($name), $attributes) . $label;
    $out .= '</label>';
    $out .= '</div>';

    return $out;
});

Form::macro('textFieldWithAddon', function ($name, $label, $content = 'pencil', $end = false, $value = null, $attributes = []) {
    $addon = '<span class="input-group-addon bg-gray">';
    $addon .= $content;
    $addon .= "</span>";

    $element = Form::text($name, old($name, $value), field_attributes($name, $attributes));

    $out = '<div class="input-group">';
    $out .= $end ? '' : $addon;
    $out .= $element;
    $out .= $end ? $addon : '';
    $out .= '</div>';

    return field_wrapper($name, $label, $out);
});

Form::macro('inputFieldWithImagePreview', function ($nameInput, $label = null, $srcImg = null, $attributesInput = [], $attributesPreview = []) {
    $namePreview = $nameInput . 'Preview';

    $attributesPreview = array_merge($attributesPreview, ['id' => $namePreview]);
    $attributesInput = array_merge($attributesInput, ['accept' => 'image/*', 'data-preview-img' => '#'.$namePreview]);

    $element = '<p>' . Html::image($srcImg, 'Pré-visualização da imagem', $attributesPreview) . '</p>';
    $element .= '<p>' . Form::file($nameInput, field_attributes($nameInput, $attributesInput, true)) . '</p>';

    if ($srcImg === null) {
        $element = str_replace('src="'.Request::root().'/"', 'src=""', $element);
    }

    return field_wrapper($nameInput, $label, $element);
});

Html::macro('menuItem', function ($name, $url, $urlActive) {
    $element = '<li class="treeview ' . active($urlActive) . '">';
    $element .= '<a href="' . $url . '">';
    $element .= '<i class="fa fa-circle-o"></i>';
    $element .= '<span>' . $name . '</span>';
    $element .= '</a>';
    $element .= '</li>';

    return $element;
});

function field_attributes($name, $attributes = [], $noClass = false)
{
    $name = str_replace('[]', '', $name);

    return array_merge(['class' => $noClass ? '' : 'form-control', 'id' => $name], $attributes);
}

function field_wrapper($name, $label, $element)
{
    $out = '<div class="form-group';
    $out .= field_error($name) . '">';
    $out .= field_label($name, $label);
    $out .= $element;
    $out .= errors_msg($name);
    $out .= '</div>';

    return $out;
}

function field_error($field)
{
    $error = '';
    if ($errors = Session::get('errors')) {
        $error = $errors->first($field) ? ' has-error' : '';
    }
    return $error;
}

function field_label($name, $label)
{
    if (is_null($label)) {
        return '';
    }

    $name = str_replace('[]', '', $name);

    $out = '<label for="' . $name . '" class="control-label">';
    $out .= $label . '</label>';

    return $out;
}

function errors_msg($field)
{
    $errors = Session::get('errors');

    if ($errors && $errors->has($field)) {
        $msg = $errors->first($field);
        return '<p class="help-block">' . $msg . '</p>';
    }

    return '';
}
