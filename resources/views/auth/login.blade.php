<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Erika Sistema</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ versioned_asset('assets/css/app.min.css') }}" type="text/css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        Erika Sistema
    </div>

    <!-- /.login-logo -->
    <div class="login-box-body">
        @include('flash::message')
        <p class="login-box-msg">Faça o login para entrar no sistema</p>

        {!! Form::open(['route' => 'signin']) !!}
          {!! Form::textField('login', 'Usuário') !!}
          {!! Form::passwordField('password', 'Senha') !!}
          {!! Form::checkboxField('remember', 'Manter conectado') !!}
          <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
        {!! Form::close() !!}
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<script src="{{ versioned_asset('assets/js/libs.min.js') }}"> </script>
<script src="{{ versioned_asset('assets/js/app.min.js') }}"></script>
</body>
</html>
