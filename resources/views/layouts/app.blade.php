<!DOCTYPE html>
<html>
  <head>
      <meta charset="UTF-8">
      <title>Erika Sistema | @yield('title', 'Home')</title>
      <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
      <link rel="stylesheet" href="{{ versioned_asset('assets/css/app.css') }}" type="text/css">
      @yield('css')
  </head>
  <body class="skin-yellow sidebar-mini">
    <div class="wrapper">
      <!-- Main Header -->
      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <b>Erika Sistema</b>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Alternar navegação</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="{!! asset('storage/images/system.ico') !!}" class="user-image" alt="Imagem do sistema"/>
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs">
                    {!! user('name') !!}
                  </span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="{!! asset('storage/images/system.ico') !!}" class="img-circle" alt="Imagem do sistema"/>
                    <p>
                      {!! user('name') !!}
                      <small>Membro desde {!! user('created_at')->format('M. Y') !!}</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                        <a href="#" class="btn btn-default btn-flat">Perfil</a>
                    </div>
                    <div class="pull-right">
                      <a href="{!! url('/logout') !!}" class="btn btn-default btn-flat">
                        Sair
                      </a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      @include('layouts.sidebar')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        @yield('content')
      </div>
      <!-- Main Footer -->
      <footer class="main-footer" style="max-height: 100px;text-align: center">
        <strong>Copyright © 2017 <a href="#">Erika Sistema</a>.</strong> All rights reserved.
      </footer>
    </div>
    <script src="{!! versioned_asset('assets/js/libs.min.js') !!}"></script>
    <script src="{!! versioned_asset('assets/js/app.min.js') !!}"></script>
  </body>
</html>
