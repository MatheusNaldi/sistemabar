@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
          Novo Produto
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'produtos.store', 'enctype' => 'multipart/form-data']) !!}
                        @include('products.fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
