@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
          Editar Produto
        </h1>
   </section>
   <div class="content">
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($product, ['route' => ['produtos.update', $product->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}

                        @include('products.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
