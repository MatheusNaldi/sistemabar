<div class="col-sm-6">
  {!! Form::textField('description', 'Descrição') !!}
</div>

<div class="col-sm-3">
  {!! Form::textField('group', 'Grupo') !!}
</div>

<div class="col-sm-3">
  {!! Form::textField('subGroup', 'Sub Grupo') !!}
</div>

<div class="col-sm-3">
  {!! Form::textField('model', 'Modelo') !!}
</div>

<div class="col-sm-3">
  {!! Form::textField('accessory', 'Acessório') !!}
</div>

<div class="col-sm-3">
  {!! Form::textField('unit', 'Unidade') !!}
</div>

<div class="col-sm-3">
  {!! Form::textField('size', 'Tamanho') !!}
</div>

<div class="col-sm-3">
  {!! Form::textField('ncm', 'NCM') !!}
</div>

<div class="col-sm-3">
  {!! Form::textField('reference', 'Referência') !!}
</div>

<div class="col-sm-6">
  {!! Form::textField('barCode', 'Código de barras') !!}
</div>

<div class="col-sm-6">
  {!! Form::inputFieldWithImagePreview(
    'picture',
    'Foto do produto',
    isset($product) ? $product->pictureUrl : null,
    [],
    ['class' => 'img-thumbnail img-auto-size'])
  !!}
</div>

<div class="col-sm-3">
  {!! Form::textFieldWithAddon('costPrice', 'Valor de custo', 'R$') !!}
</div>

<div class="col-sm-3">
  {!! Form::textFieldWithAddon('profit', 'Porcentagem de lucro', '%', true, isset($product) ? $product->profit * 100 : '0') !!}
</div>

<div class="col-sm-3">
  {!! Form::textField('retailPrice', 'Preço de varejo') !!}
</div>

<div class="col-sm-3">
  {!! Form::textField('wholesalePrice', 'Preço de atacado') !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <a href="{!! route('produtos.index') !!}" class="btn btn-default">Cancelar</a>
    <button type="submit" class="btn btn-primary pull-right">Salvar</button>
</div>
