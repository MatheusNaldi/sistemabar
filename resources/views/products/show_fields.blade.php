<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
        {!! Form::label('id', 'Id:') !!}
        <p>{!! $product->id !!}</p>
    </div>

     <div class="form-group">
        {!! Form::label('description', 'Descrição:') !!}
        <p>{!! $product->description !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('group', 'Grupo:') !!}
        <p>{!! $product->group !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('subGroup', 'Sub Grupo:') !!}
        <p>{!! $product->subGroup !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('model', 'Modelo:') !!}
        <p>{!! $product->model !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('accessory', 'Acessório:') !!}
        <p>{!! $product->accessory !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('unit', 'Unidade:') !!}
        <p>{!! $product->unit !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('size', 'Tamanho:') !!}
        <p>{!! $product->size !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('ncm', 'NCM:') !!}
        <p>{!! $product->ncm !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('reference', 'Referência:') !!}
        <p>{!! $product->reference !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('barCode', 'Código de barras:') !!}
        <p>{!! $product->barCode !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('created_at', 'Criado em:') !!}
        <p>{!! $product->present()->created_at !!}</p>
    </div>

  </div>
  <div class="col-sm-6">

    <div class="form-group">
      {!! Form::label('picture', 'Foto do produto:') !!}
      {!! Html::image($product->pictureUrl, 'Pré-visualização da imagem', ['class' => 'img-thumbnail img-auto-size']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('costPrice', 'Preço de custo:') !!}
        <p>{!! $product->present()->costPrice !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('profit', 'Porcentagem de lucro:') !!}
        <p>{!! $product->present()->profit !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('retailPrice', 'Preço de varejo:') !!}
        <p>{!! $product->present()->retailPrice !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('wholesalePrice', 'Preço de atacado:') !!}
        <p>{!! $product->present()->wholesalePrice !!}</p>
    </div>

    <div class="form-group">
        {!! Form::label('updated_at', 'Atualizado em:') !!}
        <p>{!! $product->present()->updated_at !!}</p>
    </div>
  </div>
</div>
