<table class="table table-responsive">
    <thead>
        <th>Id</th>
        <th>Descrição</th>
        <th>Grupo</th>
        <th>Preço de custo</th>
        <th>Porcentagem de lucro</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr>
            <td>{{ $product->id }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->group }}</td>
            <td>{{ $product->present()->costPrice }}</td>
            <td>{{ $product->present()->profit }}</td>
            <td>
                {!! Form::open(['route' => ['produtos.destroy', $product->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('produtos.show', [$product->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('produtos.edit', [$product->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Tem certeza?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
